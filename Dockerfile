FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8888
ADD ./target/ramosvji-configserver-0.0.1-SNAPSHOT.jar /ramosvji-configserver.jar
ENTRYPOINT ["java","-jar","/ramosvji-configserver.jar","com.ramosvji.config.RamosvjiConfigserverApplication"]